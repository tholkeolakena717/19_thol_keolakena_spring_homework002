package com.khsrd.spring_homework_002.repository;

import com.khsrd.spring_homework_002.model.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("Select * from product_tb")
    @Results(id="productMap", value = {
            @Result(property = "productId",column = "product_id"),
            @Result(property = "productName",column = "product_name"),
            @Result(property = "productPrice",column = "product_price")
    })
     List<Product> getAllProduct();
  @Select("""
          SELECT * from product_tb where product_id=#{id};
          """)
    @ResultMap("productMap")
    Product getProductById(Integer id);

    @Select("""
           insert into product_tb(product_name, product_price)
           VALUES (#{productName},#{productPrice}) RETURNING *
           """)
    @ResultMap("productMap")
    Product insertProduct(Product product);
    @Update("""
            UPDATE product_tb
            set product_name=#{product.productName},product_price=#{product.productPrice}
            where product_id=#{id};
            """)
    @ResultMap("productMap")
    void updateProduct(@PathVariable("id") Integer id, @Param("product") Product product);
    @Delete("""
            delete  from product_tb where  product_id=#{id};
            """)
    @ResultMap("productMap")
    void deleteProduct(Integer id);


//
//
//

}
