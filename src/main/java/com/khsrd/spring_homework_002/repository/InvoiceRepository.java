package com.khsrd.spring_homework_002.repository;

import com.khsrd.spring_homework_002.controller.InvoiceController;
import com.khsrd.spring_homework_002.model.Invoice;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
//    @Results(id="InvoiceMap", value = {
//
//            @Result(property = "invoceId",column = "invoice_id"),
//            @Result(property = "invoidDate",column = "invoice_date"),
//            @Result(property = "customer",column = "customer_id",one=@One(select = "com.khsrd.spring_homework_002.repository.CustomerRepository.getCustomerById"))
//
//    })
    @Select("Select * from invoice_tb")




    @Result(property = "invoceId",column = "invoice_id")
    @Result(property = "invoidDate",column = "invoice_date")
    @Result(property = "customer",column = "customer_id",one=@One(select = "com.khsrd.spring_homework_002.repository.CustomerRepository.getCustomerById"))
    @Result(property = "product",column = "product_id",many=@Many(select = "com.khsrd.spring_homework_002.repository.ProductRepository.getProductById"))
    List<Invoice>getAllInvoice();
    @Select("""
             SELECT * from invoice_tb where product_id=#{id};
            
            """)
    @Result(property = "invoceId",column = "invoice_id")
    @Result(property = "invoidDate",column = "invoice_date")
    @Result(property = "customer",column = "customer_id",one=@One(select = "com.khsrd.spring_homework_002.repository.CustomerRepository.getCustomerById"))
    @Result(property = "product",column = "product_id",many=@Many(select = "com.khsrd.spring_homework_002.repository.ProductRepository.getProductById"))

    List<Invoice> getAllInvoiceById(Integer id);
}
