package com.khsrd.spring_homework_002.repository;

import com.khsrd.spring_homework_002.model.Customer;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Results(id="CustomerMap", value = {
            @Result(property = "customerId",column = "customer_id"),
            @Result(property = "customerName",column = "customer_name"),
            @Result(property = "customerAddress",column = "customer_address"),
            @Result(property = "customerPhone",column = "customer_phone")
    })
    @Select("Select * from customer_tb")
    List<Customer> getAllCustomer();

    @Select("""
          SELECT * from customer_tb where customer_id=#{id};
          """)
    @ResultMap("CustomerMap")
    Customer getCustomerById(Integer id);
    @Select("""
           insert into customer_tb(customer_name, customer_address,customer_phone)
           VALUES (#{customerName},#{customerAddress},#{customerPhone}) RETURNING *
           """)
    @ResultMap("CustomerMap")
    Object inseretProduct(Customer customer);

@Update("""
         UPDATE customer_tb
            set customer_name=#{customer.customerName},customer_address=#{customer.customerAddress},customer_phone=#{customer.customerPhone}
            where customer_id=#{id};
        """)
@ResultMap("CustomerMap")
    void updateCustomer(Integer id, Customer customer);
    @Delete("""
            delete  from customer_tb where  customer_id=#{id};
            """)
    @ResultMap("CustomerMap")

    void deleteCustomers(Integer id);
}
