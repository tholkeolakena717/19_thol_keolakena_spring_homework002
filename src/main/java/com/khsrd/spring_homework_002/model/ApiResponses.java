package com.khsrd.spring_homework_002.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ApiResponses<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private  boolean status;
}
