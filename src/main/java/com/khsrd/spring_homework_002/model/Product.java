package com.khsrd.spring_homework_002.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int productId;
    private String productName;
    private float productPrice;
}
