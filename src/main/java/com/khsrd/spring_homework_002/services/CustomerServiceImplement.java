package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Customer;
import com.khsrd.spring_homework_002.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerServiceImplement  implements CustomerService {
    private CustomerRepository customerRepository;
   @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> getAllProduct() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Object inseretProduct(Customer customer) {
        return customerRepository.inseretProduct(customer);
    }

    @Override
    public void updateCustomer(Integer id, Customer customer) {
        customerRepository.updateCustomer(id,customer);
    }

    @Override
    public Object deleteCustomers(Integer id) {
        customerRepository.deleteCustomers(id);
        return customerRepository.getCustomerById(id);

    }


}

