package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Invoice;
import org.springframework.stereotype.Service;

import java.util.List;

public interface InvoiceServices {
    List<Invoice> getAllInvoice() ;

    List<Invoice> getAllInvoiceById(Integer id);
}
