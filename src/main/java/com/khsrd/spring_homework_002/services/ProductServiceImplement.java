package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Product;
import com.khsrd.spring_homework_002.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplement implements ProductService {
    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    private  ProductRepository productRepository;


    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product inseretProduct(Product product) {
        return productRepository.insertProduct(product);
    }

    @Override
    public void updateProduct(Integer id, Product product) {

        productRepository.updateProduct(id,product);
//        return productRepository.getProductById(id);
    }

     @Override
    public Object deleteProduct(Integer id) {
         productRepository.deleteProduct(id);
        return productRepository.getProductById(id);
    }


}
