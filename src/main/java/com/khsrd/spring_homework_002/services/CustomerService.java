package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllProduct();

    List<Customer> getAllCustomer();



    Customer getCustomerById(Integer id);

    Object inseretProduct(Customer customer);

    void updateCustomer(Integer id, Customer customer);


    Object deleteCustomers(Integer id);
}
