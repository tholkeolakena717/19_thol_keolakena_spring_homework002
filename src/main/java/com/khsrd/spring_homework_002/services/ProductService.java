package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product inseretProduct(Product product);

    void updateProduct(Integer id, Product product);


    Object deleteProduct(Integer id);
}
