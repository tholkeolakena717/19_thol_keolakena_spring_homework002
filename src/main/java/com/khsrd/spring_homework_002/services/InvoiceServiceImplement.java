package com.khsrd.spring_homework_002.services;

import com.khsrd.spring_homework_002.model.Invoice;
import com.khsrd.spring_homework_002.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class InvoiceServiceImplement  implements InvoiceServices {
    private InvoiceRepository invoiceRepository;
@Autowired
    public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public List<Invoice> getAllInvoiceById(Integer id) {
        return invoiceRepository. getAllInvoiceById(id);
    }
}
