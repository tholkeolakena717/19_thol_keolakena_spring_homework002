package com.khsrd.spring_homework_002.controller;

import com.khsrd.spring_homework_002.model.ApiResponses;
import com.khsrd.spring_homework_002.model.Customer;
import com.khsrd.spring_homework_002.model.Product;
import com.khsrd.spring_homework_002.services.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController

public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    @GetMapping("api/v1/get-all-customer")
    public ResponseEntity<?> getAllCustomer(){
        List<Customer> customers = customerService.getAllCustomer();
        return ResponseEntity.ok(
                new ApiResponses<>(
                        customers,
                        "Success",
                        true
                )
        );
    }
    @GetMapping("api/v1/product/get-all-product/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        Customer customers = customerService.getCustomerById(id);
        return ResponseEntity.ok(
                new ApiResponses<>(
                        customers,
                        "Success",
                        true
                )
        );

    }
    @PostMapping("api/v1/product/add-new-customer")
    public ResponseEntity<?> inseretCustomer(@RequestBody Customer customer){

        return ResponseEntity.ok(
                new ApiResponses<>(
                        customerService.inseretProduct(customer),
                        "Success",
                        true


                )
        );
    }
    @PutMapping("api/v1/product/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable Integer id,@RequestBody Customer customer){
      customerService.updateCustomer(id,customer);
        return ResponseEntity.ok(
                new ApiResponses<>(
                        null,
                        "Success",
                        true
                )
        );
    }
    @DeleteMapping("api/v1/product/delete-customer-by-id/{id}")
    public  ResponseEntity<?> deleteCustomers(@PathVariable Integer id){
        return ResponseEntity.ok(
                new ApiResponses<>(
                        customerService.deleteCustomers(id),
                        "Success",
                        true
                )
        );

    }
}

