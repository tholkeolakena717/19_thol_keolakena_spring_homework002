package com.khsrd.spring_homework_002.controller;

import com.khsrd.spring_homework_002.model.ApiResponses;
import com.khsrd.spring_homework_002.model.Product;
import com.khsrd.spring_homework_002.services.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductContorller {
    private final ProductService productService;

    public ProductContorller(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("api/v1/get-all-product")
    public ResponseEntity<?> getAllProduct(){
        List<Product> products = productService.getAllProduct();
        return ResponseEntity.ok(
                new ApiResponses<>(
                        products,
                        "Success",
                        true
                )
        );
    }
    //Show  Product by id
    @GetMapping("api/v1/product/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id){
        Product product = productService.getProductById(id);
        return ResponseEntity.ok(
                new ApiResponses<>(
                        product,
                        "Success",
                        true
                )
        );

    }
    //Insert product
    @PostMapping("api/v1/product/add-new-product")
    public ResponseEntity<?> inseretProduct(@RequestBody Product product){

        return ResponseEntity.ok(
                new ApiResponses<>(
                        productService.inseretProduct(product),
                        "Success",
                        true


                )
        );
    }
    //Update product
    @PutMapping("api/v1/product/update-product-by-id/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer id,@RequestBody Product product){
        productService.updateProduct(id,product);
        return ResponseEntity.ok(
                new ApiResponses<>(
                        null,
                        "Success",
                        true
                )
        );
    }
    //delete product
    @DeleteMapping("api/v1/product/delete-product-by-id/{id}")
    public  ResponseEntity<?> deleteProduct(@PathVariable Integer id){
        return ResponseEntity.ok(
                new ApiResponses<>(
                        productService.deleteProduct(id),
                        "Success",
                        true
                )
        );

    }




}
