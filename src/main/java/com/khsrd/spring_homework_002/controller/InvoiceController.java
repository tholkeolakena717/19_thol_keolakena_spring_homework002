package com.khsrd.spring_homework_002.controller;

import com.khsrd.spring_homework_002.model.ApiResponses;
import com.khsrd.spring_homework_002.model.Invoice;
import com.khsrd.spring_homework_002.services.InvoiceServices;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InvoiceController {
    private final InvoiceServices  invoiceServices;

    public InvoiceController(InvoiceServices invoiceServices) {
        this.invoiceServices = invoiceServices;
    }
    @GetMapping("api/v1/get-all-Invoice")
    public ResponseEntity<?> getAllInvoice(){
        List<Invoice> invoice =invoiceServices.getAllInvoice();
        return ResponseEntity.ok(
                new ApiResponses<>(
                        invoice,
                        "Success",
                        true
                )
        );
    }

    @GetMapping("api/v1/get-all-Invoice-id/{id}")
    public ResponseEntity<?> getAllInvoiceById(@PathVariable Integer id){
        List<Invoice> invoice =invoiceServices.getAllInvoiceById(id);
        return ResponseEntity.ok(
                new ApiResponses<>(
                        invoice,
                        "Success",
                        true
                )
        );
    }
}
